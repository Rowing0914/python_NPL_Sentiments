import nltk
import sys

class Analyzer():
    """Implements sentiment analysis."""

    def __init__(self, positives, negatives):
        """Initialize Analyzer."""

        # TODO
        self.positives = positives
        self.negatives = negatives
        
        # check inside the text file and omit the blank space
        # furthermore, delete the line starting with ";"!
        with open(positives) as f:
            for line in f:
                if not line.isspace():
                    line.strip("")
                if line.startswith(";"):
                    line = ""
        
        with open(negatives) as f:
            for line in f:
                if not line.isspace():
                    line.strip("")
                if line.startswith(";"):
                    line = ""
                

    def analyze(self, text):
        """Analyze text for sentiment, returning its score."""

        # TODO
        # initialise the counter as result
        result = 0.0
        
        # separete the text into natural language by NLTK
        tokens = nltk.tokenize.word_tokenize(text)
        print("tokens: ", tokens)
        
        # make all words start with lower case 
        for i in tokens:
            i.lower()
        
        f_negative = open("negative-words.txt", "r")
        f_positive = open("positive-words.txt", "r")
        
        # compare the tokens to dictionary of two files and add the flag number to result per Hit!!
        for word in tokens:
            print(word, end="1")
            print("")
            for line in f_negative:
                if word == line:
                    result = -1.0 + result
                    print("Hit!!")
        
        for word in tokens:
            print(word, end="1")
            print("")
            for line in f_positive:
                if word == line:
                    result = 1.0 + result
                    print("Hit!!")
                    
        # close files
        f_positive.close()
        f_negative.close()
        return result
